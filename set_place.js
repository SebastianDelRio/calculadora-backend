import mysql from 'mysql';

export default function set_place(){

   //1. conectarse a la base de datos

   let parametros={

    host:"localhost",
    user:"root",
    password:"password",
    database:"db_wallettrip" 

   }

   let conexion= mysql.createConnection(parametros);

   conexion.connect((err)=>{

    if(err){

        console.log("error en la conexion  a la base datos");
        console.log(err.message);
        return false;
    }else{

        console.log("conexionestablecida...");
        return true;
    }


   })

   //2. realizar la insercion de datos

   let consulta="INSERT INTO place (idplace, name, description) VALUES (1, 'Casa', 'Casa descriptiva')";

   conexion.query(consulta);

   //3. cerra la conexion

   conexion.end();

}